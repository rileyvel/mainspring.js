/**
 * This class manages an action that's "time buffered".
 * A client can "request" an action to be done *later*.
 * The callback will be triggered when the time's up, but if the client
 * requests the action again, the original timer will cancel.
 */

export class Mainspring {

    private _hasActiveTimer = false;
    private _timer: any;

    constructor(public options: MainspringOptions) {}

    arm(): void {

        if (this._hasActiveTimer) {
            this.cancel();
        }

        this._hasActiveTimer = true;
        this._timer = setTimeout(() => {
            this._handleTimerExpiry();
        }, this.options.delay ?? 1000);

    }

    cancel(): void {

        // sanity check
        if (!this._hasActiveTimer) {
            return;
        }

        clearTimeout(this._timer);
        this._hasActiveTimer = false;

    }

    private _handleTimerExpiry(): void {
        this._hasActiveTimer = false;
        this.options.callback();
    }

}

export interface MainspringOptions {
    callback: VoidFunction;
    delay?: number;
}
