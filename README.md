# Mainspring.js

Ah, good old debounce gets an object-oriented makeover.

## Mechanism

Once armed, the task will trigger with the given delay. However, if the main spring is armed again before time's over, the delay restarts.

## Installation

Install with npm: 

```
npm i mainspring-debounce
```

Then: 

```javascript
import { Mainspring } from 'mainspring-debounce';
```

## How to Use

First, create a mainspring object with a callback function and (optionally) a delay in MS. 

```javascript
new Mainspring({
    callback: () => {...},
    delay: 2000
});
```

Then, just call the arm() function to arm, and cancel() function to cancel.
