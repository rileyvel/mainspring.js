/**
 * This class manages an action that's "time buffered".
 * A client can "request" an action to be done *later*.
 * The callback will be triggered when the time's up, but if the client
 * requests the action again, the original timer will cancel.
 */
export class Mainspring {
    constructor(options) {
        this.options = options;
        this._hasActiveTimer = false;
    }
    arm() {
        if (this._hasActiveTimer) {
            this.cancel();
        }
        this._hasActiveTimer = true;
        this._timer = setTimeout(() => {
            this._handleTimerExpiry();
        }, this.options.delay ?? 1000);
    }
    cancel() {
        // sanity check
        if (!this._hasActiveTimer) {
            return;
        }
        clearTimeout(this._timer);
        this._hasActiveTimer = false;
    }
    _handleTimerExpiry() {
        this._hasActiveTimer = false;
        this.options.callback();
    }
}
